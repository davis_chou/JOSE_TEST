package tw.com.aitc.nimbus;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.factories.DefaultJWSVerifierFactory;
import com.nimbusds.jose.jwk.KeyType;
import com.nimbusds.jose.proc.JWSVerifierFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nimbusds.jose.jwk.KeyType.*;

public class JWSConsumer {

	public static void main(String[] args) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, JOSEException, ParseException, NoSuchProviderException {
		Map<String, KeyType> kmap = new HashMap<>();
		kmap.put("id_oct_256", OCT);
		kmap.put("id_oct_384", OCT);
		kmap.put("id_oct_512", OCT);
		kmap.put("id_rsa", RSA);
		kmap.put("id_ec", EC);

		consume("general_json", kmap);
	}

	private static void consume(String serializeType, Map<String, KeyType> kMap) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, ParseException, NoSuchProviderException {
		// ==================================================
		// 前置資料: 設定檔、...
		// ==================================================


		// ==================================================
		// JWS Object: 讀入資料
		// JWK: 金鑰相關資料
		// JWSVerifier: 驗章工具
		// ==================================================
		JWSVerifierFactory verifierFactory = new DefaultJWSVerifierFactory();
		String input = new String(Files.readAllBytes(Paths.get("output.txt")));
		boolean verify = false;
		if ("compact".equals(serializeType)) {
			JWSObject parse = JWSObject.parse(input);
			JWSHeader header = parse.getHeader();

			String hkid = header.getKeyID();
			if (kMap.containsKey(hkid)) {
				// 取得金鑰 & 產生驗章工具
				Key key = KeyInfo.getPubKey(hkid, kMap.get(hkid), "KeyStore");
				JWSVerifier jwsVerifier = verifierFactory.createJWSVerifier(header, key);

				// 驗章
				System.out.println("\n\n===== Verify JWS Object =====");
				System.out.printf("Verify alg:[%s], kid:[%s]%n", header.getAlgorithm(), hkid);
				verify = parse.verify(jwsVerifier);
			}
		}
		else if ("general_json".equals(serializeType) || "flattened_json".equals(serializeType)) {
			JWSObjectJSON parse = JWSObjectJSON.parse(input);
			List<JWSObjectJSON.Signature> signs = parse.getSignatures();

			System.out.println("\n\n===== Verify JWS Object JSON =====");
			for (JWSObjectJSON.Signature sign : signs) {
				JWSHeader header = sign.getHeader();

				String hKid = header.getKeyID();
				if (kMap.containsKey(hKid)) {
					// 取得金鑰 & 產生驗章工具
					Key key = KeyInfo.getPubKey(hKid, kMap.get(hKid), "KeyStore");
					JWSVerifier jwsVerifier = verifierFactory.createJWSVerifier(header, key);

					// 驗章
					System.out.printf("Verify alg:[%s], kid:[%s]%n", header.getAlgorithm(), header.getKeyID());
					verify = sign.verify(jwsVerifier);
				}
			}
		}

		System.out.println("verify = " + verify);
	}
}
