package tw.com.aitc.nimbus;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.util.Base64URL;
import tw.com.aitc.Utility;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;

import static com.nimbusds.jose.jwk.KeyType.*;

public class KeyInfo {
	static {
		Provider bc = BouncyCastleProviderSingleton.getInstance();
		Security.insertProviderAt(bc, 1);
		Provider[] providers = Security.getProviders();
		for (Provider provider : providers) {
			System.out.println("provider.getName() = " + provider.getName());
		}
	}

	private static String pswd = "000000";

	public static void main(String[] args) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
//		getJWK("id_oct_256", OCT, "byte[]");
//		getJWK("id_oct_384", OCT, "byte[]");
//		getJWK("id_oct_512", OCT, "byte[]");
		getJWK("id_oct_256", OCT, "KeyStore");
		getJWK("id_oct_384", OCT, "KeyStore");
		getJWK("id_oct_512", OCT, "KeyStore");
		getJWK("id_rsa", RSA, "KeyStore");
		getJWK("id_rsa", RSA, "PEM");
		getJWK("id_ec", EC, "KeyStore");
		getJWK("id_ec", EC, "PEM");
	}

	public static JWK getJWK(String kid, KeyType kty, String ksrc) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		ksrc = ksrc == null ? "KeyStore" : ksrc;
		System.out.println("========== getkey ==========");

		JWK jwk = null;
		if (kty == OCT) {
			// 舉例: byte[] 資料源
			if ("byte[]".equalsIgnoreCase(ksrc)) {
				String key = "id_oct_256".equals(kid) ? "F3D32204F2E71279D134981A0B65AA71DD0E2350942D9D626634C9C699ADF682" :
						"id_oct_384".equals(kid) ? "3B09C062CB8BBB1141B53B1C2E8C4813B9A169D586F009A832369912369B13EE7D892B41C024A24AC760BE082E2315CF" :
								"id_oct_512".equals(kid) ? "15838ABB47DAAC7C15E163381B5790E9B39493378EB40EBF43B09AD372F4E035CE48E69A765C1408B2D775EFCDD10FBC4382B3BC997245228821C94173A74AA2" :
										null;
				SecretKey secretKey = new SecretKeySpec(Utility.pack(key), "NONE");
				jwk = new OctetSequenceKey.Builder(secretKey)
						.keyID(kid)
						.build();
			}
			// 舉例: KeyStore 資料源
			else if ("KeyStore".equalsIgnoreCase(ksrc)) {
				KeyStore keyStore = Utility.loadKeyStore("oct.jceks", pswd, "jceks");
				jwk = OctetSequenceKey.load(keyStore, kid, pswd.toCharArray());
			}
			SecretKey secretKey = jwk.toOctetSequenceKey().toSecretKey();
			System.out.println("secretKey.getEncoded().length = " + secretKey.getEncoded().length);
			System.out.println("unpack(secretKey.getEncoded()) =\n" + Utility.unpack(secretKey.getEncoded()));
		}
		else if (kty == RSA) {
			// 舉例: KeyStore 資料源
			if ("KeyStore".equalsIgnoreCase(ksrc)) {
				KeyStore keyStore = Utility.loadKeyStore("rsa.p12", pswd, KeyStore.getDefaultType());
				jwk = RSAKey.load(keyStore, kid, pswd.toCharArray());
			}
			// 舉例: 讀取 PEM 檔 ( 無法解析 ENCRYPTED PRIVATE KEY，需額外依賴 org.bouncycastle:bcpkix-jdk15to18 )
			else if ("PEM".equalsIgnoreCase(ksrc)) {
				String pem = new String(Files.readAllBytes(Paths.get("rsa.pem")));
				jwk = JWK.parseFromPEMEncodedObjects(pem);
			}
			RSAKey rsaKey = jwk.toRSAKey();
			PrivateKey privateKey = rsaKey.toPrivateKey();
			System.out.println("rsaKey.size() = " + rsaKey.size());
			System.out.println("privateKey.getEncoded().length = " + privateKey.getEncoded().length);
			System.out.println("Base64URL.encode(privateKey.getEncoded()) =\n" + Base64URL.encode(privateKey.getEncoded()));
			PublicKey publicKey = rsaKey.toPublicKey();
			System.out.println("publicKey.getEncoded().length = " + publicKey.getEncoded().length);
			System.out.println("Base64URL.encode(publicKey.getEncoded()) =\n" + Base64URL.encode(publicKey.getEncoded()));
		}
		else if (kty == EC) {
			// 舉例: KeyStore 資料源
			if ("KeyStore".equalsIgnoreCase(ksrc)) {
				KeyStore keyStore = Utility.loadKeyStore("ec.p12", pswd, KeyStore.getDefaultType());
				jwk = ECKey.load(keyStore, kid, pswd.toCharArray());
			}
			// 舉例: 讀取 PEM 檔 ( 需額外依賴 org.bouncycastle:bcpkix-jdk15to18 )
			else if ("PEM".equalsIgnoreCase(ksrc)) {
				String pem = new String(Files.readAllBytes(Paths.get("ec.pem")));
				jwk = JWK.parseFromPEMEncodedObjects(pem);
			}
			ECKey ecKey = jwk.toECKey();
			PrivateKey privateKey = ecKey.toPrivateKey();
			System.out.println("ecKey.size() = " + ecKey.size());
			System.out.println("privateKey.getEncoded().length = " + privateKey.getEncoded().length);
			System.out.println("Base64URL.encode(privateKey.getEncoded()) =\n" + Base64URL.encode(privateKey.getEncoded()));
			PublicKey publicKey = ecKey.toPublicKey();
			System.out.println("publicKey.getEncoded().length = " + publicKey.getEncoded().length);
			System.out.println("Base64URL.encode(publicKey.getEncoded()) =\n" + Base64URL.encode(publicKey.getEncoded()));
		}

		System.out.println("----------");
		if (jwk != null) {
			System.out.println("jwk.getKeyID() = " + jwk.getKeyID());
			System.out.println("jwk.getKeyType() = " + jwk.getKeyType());
			System.out.println("jwk.getAlgorithm() = " + jwk.getAlgorithm());
			System.out.println("jwk.getKeyUse() = " + jwk.getKeyUse());
			System.out.println("jwk.isPrivate() = " + jwk.isPrivate());
			System.out.println("jwk =\n" + jwk);
			System.out.println("jwk.toPublicJWK() =\n" + jwk.toPublicJWK());
		}
		return jwk;
	}

	public static Key getPubKey(String kid, KeyType kty, String ksrc) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		JWK jwk = getJWK(kid, kty, ksrc);
		Key key = null;
		if (kty == OCT) {
			key = jwk.toOctetSequenceKey().toSecretKey();
		}
		else if (kty == RSA) {
			key = jwk.toRSAKey().toPublicKey();
		}
		else if (kty == EC) {
			key = jwk.toECKey().toPublicKey();
		}
		return key;
	}

	public static Key getPriKey(String kid, KeyType kty, String ksrc) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		JWK jwk = getJWK(kid, kty, ksrc);
		Key key = null;
		if (kty == OCT) {
			key = jwk.toOctetSequenceKey().toSecretKey();
		}
		else if (kty == RSA) {
			key = jwk.toRSAKey().toPrivateKey();
		}
		else if (kty == EC) {
			key = jwk.toECKey().toPrivateKey();
		}
		return key;
	}
}
