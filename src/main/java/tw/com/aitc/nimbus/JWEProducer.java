package tw.com.aitc.nimbus;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jose.crypto.impl.*;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nimbusds.jose.EncryptionMethod.A256GCM;
import static com.nimbusds.jose.HeaderParameterNames.*;
import static com.nimbusds.jose.JWEAlgorithm.ECDH_ES_A128KW;
import static com.nimbusds.jose.jwk.KeyType.*;
import static java.nio.file.StandardOpenOption.*;

public class JWEProducer {
	public static void main(String[] args) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		List<Map<String, Object>> encInfo = new ArrayList<>();
//		Map<String, Object> enc1 = new HashMap<>();
//		enc1.put(ALGORITHM, DIR);
//		enc1.put(ENCRYPTION_ALGORITHM, A256GCM);
//		enc1.put(KEY_ID, "id_oct_256");
//		enc1.put("ksrc", "KeyStore");
//		encInfo.add(enc1);

//		Map<String, Object> enc2 = new HashMap<>();
//		enc2.put(ALGORITHM, RSA_OAEP_256);
//		enc2.put(ENCRYPTION_ALGORITHM, A256GCM);
//		enc2.put(KEY_ID, "id_rsa");
//		enc2.put("ksrc", "KeyStore");
//		encInfo.add(enc2);

		Map<String, Object> enc3 = new HashMap<>();
		enc3.put(ALGORITHM, ECDH_ES_A128KW);
		enc3.put(ENCRYPTION_ALGORITHM, A256GCM);
		enc3.put(KEY_ID, "id_ec");
		enc3.put("ksrc", "KeyStore");
		encInfo.add(enc3);

//		Map<String, Object> enc5 = new HashMap<>();
//		enc5.put(ALGORITHM, PBES2_HS256_A128KW);
//		enc5.put(ENCRYPTION_ALGORITHM, A256GCM);
//		enc5.put("p2pswd", "000000");
//		enc5.put(PBES2_SALT_INPUT, "12345678");
//		enc5.put(PBES2_COUNT, "1000");
//		encInfo.add(enc5);

		produce("compact", encInfo);
//		produce("general_json", encInfo);
//		produce("flattened_json", encInfo);
	}

	private static void produce(String serializeType, List<Map<String, Object>> encInfo) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		// ==================================================
		// 前置資料: 設定檔、...
		// ==================================================

		// ==================================================
		// JWK: 金鑰相關資料
		// JWEEncrypter: 加密工具
		// JWE Header
		// ==================================================
		int eiLen = encInfo.size();
		JWEEncrypter[] jweEncrypters = new JWEEncrypter[eiLen];
		JWEHeader[] jweHeaders = new JWEHeader[eiLen];

		for (int i = 0; i < eiLen; i++) {
			System.out.println("\n===== Prepare JWK & Signer " + i + " =====");
			JWEAlgorithm alg = (JWEAlgorithm) encInfo.get(i).get(ALGORITHM);
			String kid = (String) encInfo.get(i).get(KEY_ID);
			EncryptionMethod enc = (EncryptionMethod) encInfo.get(i).get(ENCRYPTION_ALGORITHM);
			String ksrc = (String) encInfo.get(i).get("ksrc");
			String p2pswd = (String) encInfo.get(i).get("p2pswd");
			int p2s = (int) encInfo.get(i).get(PBES2_SALT_INPUT);
			int p2c = (int) encInfo.get(i).get(PBES2_COUNT);

			System.out.println("jwsAlg = " + alg);
			System.out.println("----------");
			// DirectCrypto
			if (DirectCryptoProvider.SUPPORTED_ALGORITHMS.contains(alg) &&
					DirectCryptoProvider.SUPPORTED_ENCRYPTION_METHODS.contains(enc)) {
				System.out.println("DirectCrypto");
				jweEncrypters[i] = new DirectEncrypter(((SecretKey) KeyInfo.getPubKey(kid, OCT, ksrc)));
				jweHeaders[i] = new JWEHeader.Builder(alg, enc)
						.keyID(kid)
						.build();
			}
			// AESCrypto
			if (AESCryptoProvider.SUPPORTED_ALGORITHMS.contains(alg) &&
					AESCryptoProvider.SUPPORTED_ENCRYPTION_METHODS.contains(enc)) {
				System.out.println("AESCrypto");
				jweEncrypters[i] = new AESEncrypter(((SecretKey) KeyInfo.getPubKey(kid, OCT, ksrc)));
				jweHeaders[i] = new JWEHeader.Builder(alg, enc)
						.keyID(kid)
						.build();
			}
			// RSACrypto
			if (RSACryptoProvider.SUPPORTED_ALGORITHMS.contains(alg) &&
					RSACryptoProvider.SUPPORTED_ENCRYPTION_METHODS.contains(enc)) {
				System.out.println("RSACrypto");
				jweEncrypters[i] = new RSAEncrypter(((RSAPublicKey) KeyInfo.getPubKey(kid, RSA, ksrc)));
				jweHeaders[i] = new JWEHeader.Builder(alg, enc)
						.keyID(kid)
						.build();
			}
			// ECDHCrypto
			if (ECDHCryptoProvider.SUPPORTED_ALGORITHMS.contains(alg) &&
					ECDHCryptoProvider.SUPPORTED_ENCRYPTION_METHODS.contains(enc)) {
				System.out.println("ECDHCrypto");
				jweEncrypters[i] = new ECDHEncrypter(((ECPublicKey) KeyInfo.getPubKey(kid, EC, ksrc)));
				jweHeaders[i] = new JWEHeader.Builder(alg, enc)
						.keyID(kid)
						.build();
			}
			// PasswordBasedCrypto
			if (PasswordBasedCryptoProvider.SUPPORTED_ALGORITHMS.contains(alg) &&
					PasswordBasedCryptoProvider.SUPPORTED_ENCRYPTION_METHODS.contains(enc)) {
				System.out.println("PasswordBasedCrypto");
				jweEncrypters[i] = new PasswordBasedEncrypter(p2pswd, p2s, p2c);
				jweHeaders[i] = new JWEHeader.Builder(alg, enc)
						.build();
			}
		}


		// ==================================================
		// JWE Object: 填入資料與加密
		// ==================================================
		System.out.println("\n\n===== Prepare JWE Object =====");

		Payload payload = new Payload(Files.readAllBytes(Paths.get("payload.json")));
		System.out.println("\npayload =\n" + payload);

		String output = null;
		if ("compact".equalsIgnoreCase(serializeType)) {
			System.out.println("jweHeader =\n" + jweHeaders[0]);

			JWEObject jweObject = new JWEObject(jweHeaders[0], payload);
			System.out.println("\n\n===== Encrypt JWE Object =====");
			jweObject.encrypt(jweEncrypters[0]);
			System.out.println("jweObject.getState() = " + jweObject.getState());
			System.out.println("jweHeader =\n" + jweObject.getHeader());
			output = jweObject.serialize();
		}

		System.out.println("output =\n" + output);
		Files.write(Paths.get("output.txt"), output.getBytes(),
				CREATE, TRUNCATE_EXISTING, WRITE);
	}
}
