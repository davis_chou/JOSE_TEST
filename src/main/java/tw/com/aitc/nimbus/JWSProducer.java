package tw.com.aitc.nimbus;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.factories.DefaultJWSSignerFactory;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.produce.JWSSignerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nimbusds.jose.HeaderParameterNames.ALGORITHM;
import static com.nimbusds.jose.HeaderParameterNames.KEY_ID;
import static com.nimbusds.jose.JWSAlgorithm.*;
import static com.nimbusds.jose.jwk.KeyType.*;
import static java.nio.file.StandardOpenOption.*;

public class JWSProducer {
	public static void main(String[] args) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		List<Map<String, Object>> signInfo = new ArrayList<>();
		Map<String, Object> sign1 = new HashMap<>();
		sign1.put(ALGORITHM, HS256);
		sign1.put(KEY_ID, "id_oct_256");
		sign1.put("ksrc", "KeyStore");
		signInfo.add(sign1);

		Map<String, Object> sign2 = new HashMap<>();
		sign2.put(ALGORITHM, RS256);
		sign2.put(KEY_ID, "id_rsa");
		sign2.put("ksrc", "KeyStore");
		signInfo.add(sign2);

		Map<String, Object> sign3 = new HashMap<>();
		sign3.put(ALGORITHM, ES256);
		sign3.put(KEY_ID, "id_ec");
		sign3.put("ksrc", "KeyStore");
		signInfo.add(sign3);

//		produce("compact", signInfo);
		produce("general_json", signInfo);
//		produce("flattened_json", signInfo);
	}

	private static void produce(String serializeType, List<Map<String, Object>> signInfo) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, JOSEException, NoSuchProviderException {
		// ==================================================
		// 前置資料: 設定檔、...
		// ==================================================

		// ==================================================
		// JWK: 金鑰相關資料
		// JWSSigner: 簽章工具
		// ==================================================
		JWSSignerFactory signerFactory = new DefaultJWSSignerFactory();
		int siLen = signInfo.size();
		JWK[] jwks = new JWK[siLen];
		JWSSigner[] jwsSigners = new JWSSigner[siLen];

		for (int i = 0; i < siLen; i++) {
			System.out.println("\n===== Prepare JWK & Signer " + i + " =====");
			JWSAlgorithm alg = (JWSAlgorithm) signInfo.get(i).get(ALGORITHM);
			String kid = (String) signInfo.get(i).get(KEY_ID);
			String ksrc = (String) signInfo.get(i).get("ksrc");

			System.out.println("jwsAlg = " + alg);
			System.out.println("----------");
			// HMAC
			if (JWSAlgorithm.Family.HMAC_SHA.contains(alg)) {
				System.out.println("HMAC");
				jwks[i] = KeyInfo.getJWK(kid, OCT, ksrc);
			}
			// RSA 公私鑰
			else if (JWSAlgorithm.Family.RSA.contains(alg)) {
				System.out.println("RSA");
				jwks[i] = KeyInfo.getJWK(kid, RSA, ksrc);
			}
			// EC 公私鑰
			else if (JWSAlgorithm.Family.EC.contains(alg)) {
				System.out.println("EC");
				jwks[i] = KeyInfo.getJWK(kid, EC, ksrc);
			}

			// 產生簽章工具
			if (jwks[i] != null) {
				jwsSigners[i] = signerFactory.createJWSSigner(jwks[i]);
			}
		}


		// ==================================================
		// JWS Object: 填入資料與簽章
		// ==================================================
		System.out.println("\n\n===== Prepare JWS Object =====");

		Payload payload = new Payload(Files.readAllBytes(Paths.get("payload.json")));
		System.out.println("\npayload =\n" + payload);

		String output = null;
		if ("compact".equalsIgnoreCase(serializeType)) {
			JWSHeader jwsHeader = new JWSHeader.Builder((JWSAlgorithm) signInfo.get(0).get(ALGORITHM))
					.keyID((String) signInfo.get(0).get(KEY_ID))
					.build();
			System.out.println("jwsHeader =\n" + jwsHeader);

			JWSObject jwsObject = new JWSObject(jwsHeader, payload);
			System.out.println("\n\n===== Sign JWS Object =====");
			jwsObject.sign(jwsSigners[0]);
			System.out.println("jwsObject.getState() = " + jwsObject.getState());
			output = jwsObject.serialize();
		}

		else if ("general_json".equals(serializeType) || "flattened_json".equals(serializeType)) {
			JWSObjectJSON jwsObjectJSON = new JWSObjectJSON(payload);

			for (int i = 0; i < siLen; i++) {
				System.out.println("\n\n===== Sign JWS Object JSON " + i + " =====");
				JWSHeader jwsHeader = new JWSHeader.Builder((JWSAlgorithm) signInfo.get(i).get(ALGORITHM))
						.keyID((String) signInfo.get(i).get(KEY_ID))
						.build();
				System.out.println("jwsHeader " + i + " =\n" + jwsHeader);

				UnprotectedHeader unprotectedHeader = new UnprotectedHeader.Builder()
						.param("test", "Signer " + i)
						.build();
				System.out.println("unprotectedHeader " + i + " =\n" + unprotectedHeader);

				jwsObjectJSON.sign(jwsHeader, unprotectedHeader, jwsSigners[i]);
			}
			System.out.println("jwsObjectJSON.getState() = " + jwsObjectJSON.getState());
			output = serializeType.startsWith("general") ?
					jwsObjectJSON.serializeGeneral() : jwsObjectJSON.serializeFlattened();

		}

		System.out.println("output =\n" + output);
		Files.write(Paths.get("output.txt"), output.getBytes(),
				CREATE, TRUNCATE_EXISTING, WRITE);
	}
}
