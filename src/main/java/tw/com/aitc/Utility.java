package tw.com.aitc;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

public class Utility {
	public static String unpack(byte[] srcBytes) {
		StringBuilder sb = new StringBuilder();
		String byteStr = null;

		for (byte srcByte : srcBytes) {
			byteStr = Integer.toHexString(srcByte & 0xFF);
			if (byteStr.length() == 1)
				sb.append("0");
			sb.append(byteStr.toUpperCase());
		}

		return sb.toString();
	}

	public static byte[] pack(String sSrc) {
		byte[] b = null;
		String sSrc1 = null;
		int byteArrayLength;
		byte b0;

		if (sSrc == null)
			return b;

		sSrc1 = (sSrc.length() % 2) == 1 ? "0" + sSrc : sSrc;
		byteArrayLength = sSrc1.length() / 2;
		b = new byte[byteArrayLength];

		for (int i = 0; i < byteArrayLength; i++) {
			b0 = (byte) Integer.valueOf(sSrc1.substring(i * 2, i * 2 + 2), 16).intValue();
			b[i] = b0;
		}

		return b;
	}

	public static KeyStore loadKeyStore(String keyStore, String pswd, String storeType) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, NoSuchProviderException {
		try (FileInputStream fis = new FileInputStream(keyStore)) {
			KeyStore ks = KeyStore.getInstance(storeType);
			System.out.println("ks.getProvider() = " + ks.getProvider());
			char[] pwd = pswd.toCharArray();
			ks.load(fis, pwd);
			return ks;
		}
	}
}
